# Open Warcraft #

Open Warcraft is an offline, single-player 2D experience based on the Warcraft universe. Using resources from Wowhead, this project is designed to take the World of Warcraft experience offline with the main focus of single-player experiences.

### What is this repository for? ###

This repository is in very early stages. This is the client, which will eventually include all the scripts for creatures, quests, spells, and more.

Features will eventually include the following:

* Quests - exactly as they are in the current version of the online game.
* Creatures - same as above.
* Classes - same as above.
* Talents - same as above.

None of the multiplayer features of Warcraft will be included, such as Dungeons and Raids, but there will be features more customised for an end-game single-player experience. These are yet to be planned.

### How do I get set up? ###

If you just wish to download the latest client and data, there will be a download available for when the first stable version is released.

If you wish to contribute, you can do so by writing a pull request. Please follow the below guidelines. In order to build and test, you will require several items:

* Haxe Toolkit 3.2 or later installed.
* OpenFL 3.4 and Lime 2.7 or later (installed via haxelib using `haxelib install` command); `msignal` 1.2.2 or later; `hscript` 2.0.4 or later.
* MySQL Server 5.4.
* Any operating system that supports the above requirements.

### Contribution guidelines ###

Since this is in very early stages, we are currently not allowing pull requests. Once it reaches Beta stage and bugs are being ironed out, we will then open up pull requests to the public and allow contribution.

The reason for this is because dramatic changes will be taking affect during Alpha which are being tested and to the liking of the core team. Anyone who is not a member of the core team will be unable to contribute for this reason.