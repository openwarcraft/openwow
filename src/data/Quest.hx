package data;

import sys.db.Object;
import sys.db.Types;

@:id(EntryID)
@:table("quest_template")
class Quest extends Object
{
	
	public var EntryID:SInt;
	public var ZoneOrSort:SInt;
	public var Title:SString<128>;
	public var StartNPC:SInt;
	public var EndNPC:SInt;
	public var Type:SInt;
	public var RecommendedLevel:SInt;
	public var Repeatable:SBool;
	public var RepeatType:SInt;
	public var RequiredLevel:SInt;
	public var RequiredCompletedQuest1:SInt;
	public var RequiredCompletedQuest2:SInt;
	public var RequiredCompletedQuest3:SInt;
	public var RequiredCompletedQuest4:SInt;
	public var RequiredRepId:SInt;
	public var RequiredRepStanding:SInt;
	public var RequiredSkill:SInt;
	public var RequiredSkillLevel:SInt;
	public var AllowableClass:SInt;
	public var AllowableRace:SInt;
	public var Details:SString<1024>;
	public var Objectives:SString<1024>;
	public var Incomplete:SString<1024>;
	public var Complete:SString<1024>;
	public var SourceItemId:SInt;
	public var SourceItemAmount:SInt;
	public var RequiredItemId1:SInt;
	public var RequiredItemId2:SInt;
	public var RequiredItemId3:SInt;
	public var RequiredItemId4:SInt;
	public var RequiredItemAmount1:SInt;
	public var RequiredItemAmount2:SInt;
	public var RequiredItemAmount3:SInt;
	public var RequiredItemAmount4:SInt;
	public var RequiredMobId1:SInt;
	public var RequiredMobId2:SInt;
	public var RequiredMobId3:SInt;
	public var RequiredMobId4:SInt;
	public var RequiredMobAmount1:SInt;
	public var RequiredMobAmount2:SInt;
	public var RequiredMobAmount3:SInt;
	public var RequiredMobAmount4:SInt;
	public var RewardItemId1:SInt;
	public var RewardItemId2:SInt;
	public var RewardItemId3:SInt;
	public var RewardItemId4:SInt;
	public var RewardItemAmount1:SInt;
	public var RewardItemAmount2:SInt;
	public var RewardItemAmount3:SInt;
	public var RewardItemAmount4:SInt;
	public var RewardChoiceItemId1:SInt;
	public var RewardChoiceItemId2:SInt;
	public var RewardChoiceItemId3:SInt;
	public var RewardChoiceItemId4:SInt;
	public var RewardChoiceItemId5:SInt;
	public var RewardChoiceItemId6:SInt;
	public var RewardChoiceItemAmount1:SInt;
	public var RewardChoiceItemAmount2:SInt;
	public var RewardChoiceItemAmount3:SInt;
	public var RewardChoiceItemAmount4:SInt;
	public var RewardChoiceItemAmount5:SInt;
	public var RewardChoiceItemAmount6:SInt;
	public var RewardRepId1:SInt;
	public var RewardRepId2:SInt;
	public var RewardRepId3:SInt;
	public var RewardRepId4:SInt;
	public var RewardRepAmount1:SInt;
	public var RewardRepAmount2:SInt;
	public var RewardRepAmount3:SInt;
	public var RewardRepAmount4:SInt;
	public var RewardTitle:SInt;
	public var RewardSpellCast:SInt;
	public var RewardExp:SInt;
	public var RewardHonor:SInt;
	public var RewardMoney:SInt;
	public var RewardMoneyAtMax:SInt;
	public var SourceItemRemove:SBool;
	
	public function new()
	{
		super();
	}
	
	public static var getQuestById(id:Int):Quest
	{
		return Quest.manager.get(id);
	}
	
}