package;

import data.Quest;

import openfl.display.Sprite;
import openfl.Lib;
import openfl.Assets;
import haxe.Json;

import sys.db.Mysql;
import sys.db.Manager;
import sys.db.TableCreate;

class Main extends Sprite 
{

	public function new() 
	{
		super();
		
		setupTables("data/connection.json");
	}
	
	private function setupTables(conInfo:String)
	{
		var connection:Dynamic = Json.parse(Assets.getText(conInfo));
		
		Manager.cnx = Mysql.connect( {
			host: connection.host,
			port: connection.port,
			user: connection.user,
			pass: connection.pass,
			database: "openwow"
		});
		
		if (!TableCreate.exists(Quest.manager))
			TableCreate.create(Quest.manager);
		
		
	}

}
